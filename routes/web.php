<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('hasAllowedIPAddress')->group(function () {


    Auth::routes();


    Route::prefix('/dashboard')->middleware('isAdminUser')->group(function () {
        Route::get('/', 'Admin\DashboardController@index')->name('dashboard');

        // Users
        Route::prefix('users')->name('users.')->group(function () {
            Route::get('/', 'Admin\UsersController@index')->name('index');
            Route::get('/create', 'Admin\UsersController@create')->name('create');
            Route::post('/store', 'Admin\UsersController@store')->name('store');
            Route::get('/edit/{id}', 'Admin\UsersController@edit')->name('edit');
            Route::post('/update/{id}', 'Admin\UsersController@update')->name('update');
            Route::get('/delete/{id}', 'Admin\UsersController@delete')->name('delete');
        });

        // Products
        Route::prefix('products')->name('products.')->group(function () {

            Route::get('/', 'Admin\ProductsController@index')->name('index');
            Route::get('/create', 'Admin\ProductsController@create')->name('create');
            Route::post('/store', 'Admin\ProductsController@store')->name('store');
            Route::get('/edit/{id}', 'Admin\ProductsController@edit')->name('edit');
            Route::post('/update/{id}', 'Admin\ProductsController@update')->name('update');
            Route::get('/delete/{id}', 'Admin\ProductsController@delete')->name('delete');
            Route::get('/get-products-index-table/{id}/{search}', 'Admin\ProductsController@getProductsIndexTable')->name('get_products_index_table');
        });

        // Categories
        Route::prefix('categories')->name('categories.')->group(function () {

            Route::get('/', 'Admin\CategoriesController@index')->name('index');
            Route::get('/create', 'Admin\CategoriesController@create')->name('create');
            Route::post('/store', 'Admin\CategoriesController@store')->name('store');
            Route::get('/edit/{id}', 'Admin\CategoriesController@edit')->name('edit');
            Route::post('/update/{id}', 'Admin\CategoriesController@update')->name('update');
            Route::get('/delete/{id}', 'Admin\CategoriesController@delete')->name('delete');
            Route::get('/get-categories-index-table/{search}', 'Admin\CategoriesController@getCategoriesIndexTable')->name('get_categories_index_table');
        });

        // Vats
        Route::prefix('vats')->name('vats.')->group(function () {

            Route::get('/', 'Admin\VatsController@index')->name('index');
            Route::get('/create', 'Admin\VatsController@create')->name('create');
            Route::post('/store', 'Admin\VatsController@store')->name('store');
            Route::get('/edit/{vat}', 'Admin\VatsController@edit')->name('edit');
            Route::post('/update/{vat}', 'Admin\VatsController@update')->name('update');
            Route::get('/delete/{vat}', 'Admin\VatsController@delete')->name('delete');
            Route::get('/get-vats-index-table/{search}', 'Admin\VatsController@getVatsIndexTable')->name('get_vats_index_table');
        });

        Route::prefix('orders')->name('orders.')->group(function () {
            Route::get('/', 'Admin\OrdersController@index')->name('index');
        });

        // Allergies
        Route::prefix('allergies')->name('allergies.')->group(function () {

            Route::get('/', 'Admin\AllergiesController@index')->name('index');
            Route::get('/create', 'Admin\AllergiesController@create')->name('create');
            Route::post('/store', 'Admin\AllergiesController@store')->name('store');
            Route::get('/edit/{allergy}', 'Admin\AllergiesController@edit')->name('edit');
            Route::post('/update/{allergy}', 'Admin\AllergiesController@update')->name('update');
            Route::get('/delete/{allergy}', 'Admin\AllergiesController@delete')->name('delete');
            Route::get('/get-index-table/{search}', 'Admin\AllergiesController@getIndexTable')->name('get_index_table');
        });

        // Discounts
        Route::prefix('discounts')->name('discounts.')->group(function () {

            Route::get('/', 'Admin\DiscountsController@index')->name('index');
            Route::get('/create', 'Admin\DiscountsController@create')->name('create');
            Route::post('/store/{product}', 'Admin\DiscountsController@store')->name('store');
            Route::get('/edit/{product}', 'Admin\DiscountsController@edit')->name('edit');
            Route::post('/update/{product}', 'Admin\DiscountsController@update')->name('update');
        });
    });

    // Orders
    Route::prefix('orders')->name('orders.')->group(function () {

        Route::get('/success/{order}', 'OrdersController@success')->name('success');
        Route::post('/store', 'OrdersController@store')->name('store');
        Route::post('/update/{order}', 'OrdersController@update')->name('update');
    });

    // Payments
    Route::prefix('payments')->name('payments.')->group(function () {

        Route::get('/prepare-payment/{order}', 'PaymentsController@preparePayment')->name('prepare');
        Route::get('/complete', 'PaymentsController@redirect')->name('redirect');
    });


// Webshop
    Route::get('/', 'HomeController@index')->name('index');

// Categories
    Route::get('/{category}', 'CategoriesController@category')->name('category');

// Cart
    Route::prefix('cart')->name('cart.')->group(function () {
        Route::post('/store', 'CartController@store')->name('store');
    });

});


// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');


require __DIR__ . '/auth.php';
