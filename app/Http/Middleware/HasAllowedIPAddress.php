<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HasAllowedIPAddress
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $allowedIpAddresses = [
            '84.105.53.94',
             '192.168.10.1'
        ];

        if (in_array($_SERVER['REMOTE_ADDR'], $allowedIpAddresses)) {
            return $next($request);
        }

        abort(404);
    }
}
