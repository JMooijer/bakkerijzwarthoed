<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\StoreOrderRequest;
use App\Http\Requests\Orders\UpdateOrderRequest;

class OrdersController extends Controller
{
    /**
     * Show index
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('orders.index');
    }

    /**
     * Store order
     * 
     * @param StoreOrderRequest $request
     */
    public static function store(StoreOrderRequest $request) {

        $order = new Order();
        $order->fill($request->only($order->getFillable()));
        $order->save();
    }

    /**
     * Update order
     * 
     * @param UpdateOrderRequest $request
     * @param Order $order
     */
    public static function update(UpdateOrderRequest $request, Order $order) {

        $order->payment_status  = $request->payment_status;
        $order->mollie_id       = $request->mollie_id;
        $order->paid_at         = $request->paid_at;
        $order->amount          = $request->amount;
        $order->save();
    }
}
