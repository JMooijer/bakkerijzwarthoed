<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mollie\Laravel\Facades\Mollie;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Classes\PaymentStatus;

class PaymentsController extends Controller
{
    /**
     * Prepare payment
     *
     * @param Order $order
     *
     * @return redirect
     */
    public function preparePayment(Order $order)
    {
        $payment = Mollie::api()->payments->create(array(

            'amount' => array(

                'currency'  => 'EUR',
                'value'     => $order->amount,
            ),
            'description'   => 'Order #'.$order->id,
            'redirectUrl'   => 'http://bakkerijzwarthoed.homestead/payments/complete',
            'webhookUrl'    => 'https://8f24-84-107-254-104.ngrok.io/api/payments/webhook',
            'metadata'      => array(

                'order_id'  => $order->id,
            ),
        ));

        session(['br_order_id' => $order->id]);

        $order->mollie_id       = $payment->id;
        $order->payment_status  = PaymentStatus::OPEN;
        $order->save();

        // redirect customer to Mollie checkout page
        return redirect($payment->getCheckoutUrl(), 303);
    }

    /**
     * Fetch, check and process payment
     *
     * @param Request $request
     */
    public function handleWebhookNotification(Request $request)
    {
        $paymentId  = $request->id;
        $payment    = Mollie::api()->payments->get($paymentId);
        $order      = Order::find($payment->metadata->order_id);

        if ($payment->isPaid()) {

            $order->payment_status  = PaymentStatus::PAID;
            $order->paid_at         = $payment->paidAt;
        } elseif ($payment->isOpen()) {

            $order->payment_status = PaymentStatus::OPEN;
        } elseif ($payment->isPending()) {

            $order->payment_status = PaymentStatus::PENDING;
        } elseif ($payment->isFailed()) {

            $order->payment_status = PaymentStatus::FAILED;
        } elseif ($payment->isExpired()) {

            $order->payment_status = PaymentStatus::EXPIRED;
        } elseif ($payment->isCanceled()) {

            $order->payment_status = PaymentStatus::CANCELED;
        } elseif ($payment->hasRefunds()) {

            $order->payment_status = PaymentStatus::PAID;
        } elseif ($payment->hasChargebacks()) {

            $order->payment_status = PaymentStatus::PAID;
        }

        $order->save();
    }

    /**
     * Show thank you page
     *
     * @return \Illuminate\View\View
     */
    public function redirect()
    {
        $order      = Order::find(session('br_order_id'));
        $payment    = Mollie::api()->payments->get($order->mollie_id);

        dd($order, $payment);
    }
}
