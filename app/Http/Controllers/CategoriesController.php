<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    // Show index
    public function index()
    {
        return view('site.categories.index')->with('categories', Category::orderBy('title', 'ASC')->get());
    }

    // show category detail page
    public function category(Category $category)
    {
        $category->load(['products']);

        return view('site.categories.category')->with([
            'category'      => $category,
            'categories'    => Category::orderBy('title', 'ASC')->get(),
        ]);
    }
}
