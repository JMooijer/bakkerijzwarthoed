<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    public function store(Request $request)
    {
        $product = Product::findOrFail($request->input('product_id'));

        Cart::add(
            $product->id,
            $product->title,
            $request->input('quantity'),
            $product->price,
        );

        Cart::add(
            35,
            $product->title,
            $request->input('quantity'),
            $product->price,
        );

        return redirect()->route('index')->with([
            'message' => 'Product(en) toegevoegd',
            'status' => 'success'
        ]);
    }
}
