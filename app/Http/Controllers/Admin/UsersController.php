<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Users\StoreUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class UsersController extends Controller
{
    /**
     * Show index
     *
     * @return View
     */
    public function index()
    {
        return view('admin.users.index')->with('users', User::all());
    }

    /**
     * Create
     *
     * @return View
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Edit
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id)
    {
        return view('admin.products.edit')->with([

            'user' => User::find($id),
        ]);
    }

    /**
     * Store
     *
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreUserRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('users.index')->with('message', 'Gebruiker toegevoegd');
    }

    /**
     * Update
     *
     * @param StoreUserRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreUserRequest $request, int $id)
    {
        $user = User::find($id);
        $user->update($request->only($user->getFillable()));
        $user->save();

        return redirect()->route('users.index')->with('message', 'Gebruiker is gewijzigd');
    }

    /**
     * Delete
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(int $id)
    {
        User::find($id)->delete();

        return redirect()->route('users.index')->with('message', 'Gebruiker is verwijderd');
    }

    /**
     * Get index table
     *
     * @param String $search
     * @return string
     */
    public function getUsersIndexTable(string $search)
    {
        $users = User::search($search)
            ->orderBy('title', 'ASC')
            ->get();

        $html = view('admin.partials.users.index_table', compact('users'))->render();

        return $html;
    }
}
