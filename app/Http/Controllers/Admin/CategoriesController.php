<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Http\Requests\Categories\StoreCategoryRequest;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    // Show index
    public function index()
    {
        return view('admin.categories.index')->with('categories', Category::orderBy('title', 'ASC')->get());
    }

    /**
     * Create new category
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Edit category
     *
     * @param int $id
     * 
     * @return \Illuminate\View\View
     */
    public function edit(int $id)
    {
        return view('admin.categories.edit')->with('category', Category::find($id));
    }

    /**
     * Store new category
     *
     * @param StoreCategoryRequest $request
     * @return \Illuminate\View\View
     */
    public function store(StoreCategoryRequest $request)
    {
        $category = new Category();
        $category->fill($request->only($category->getFillable()));
        $category->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $path = request()->file('image')->storeAs('images/categories', $category->title.'_'.$category->id.'.'.$request->file('image')->getClientOriginalExtension(), 'public');
            
            $category->image = $path;
            $category->save();
        }
        
        return redirect()->route('categories.index')->with('message', 'Categorie toegevoegd');
    }

    /**
     * Update category
     *
     * @param StoreCategoryRequest $request
     * @param int $id
     */
    public function update(StoreCategoryRequest $request, int $id)
    {
        $category = Category::find($id);
        $category->update($request->only($category->getFillable()));
        $category->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $path = request()->file('image')->storeAs('images/categories', $category->title.'_'.$category->id.'.'.$request->file('image')->getClientOriginalExtension(), 'public');
            
            $category->image = $path;
            $category->save();
        }
        
        return redirect()->route('categories.index')->with('message', 'Categorie is gewijzigd');
    }


    /**
     * Delete category
     *
     * @param int $id
     */
    public function delete(int $id)
    {
        Product::where('category_id', $id)->update(['category_id' => null]);
        Category::find($id)->delete();

        return redirect()->route('categories.index')->with('message', 'Categorie is verwijderd');
    }

    /**
     * Get categories index table
     *
     * @param String $search
     */
    public function getCategoriesIndexTable(String $search)
    {
        $categories = Category::search($search)->orderBy('title', 'ASC')->get();
                    
        $html = view('admin.partials.categories.index_table', compact('categories'))->render();

        return $html;
    }
}
