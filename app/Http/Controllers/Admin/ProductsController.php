<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Vat;
use App\Models\Allergy;
use App\Http\Requests\Products\StoreProductRequest;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Show all products
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.products.index')->with([
            
            'products'      => Product::with(['category', 'vat'])->orderBy('title', 'ASC')->get(),
            'categories'    => Category::whereHas('products')->orderBy('title', 'ASC')->get(),
        ]);
    }

    /**
     * Create new product
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.products.create')->with([
            
            'categories'    => Category::orderBy('title', 'ASC')->get(),
            'vats'          => Vat::orderBy('rate', 'ASC')->orderBy('title', 'ASC')->get(),
            'allergies'     => Allergy::all()
        ]);
    }

    /**
     * Edit product
     *
     * @param int $id
     * 
     * @return \Illuminate\View\View
     */
    public function edit(int $id)
    {
        return view('admin.products.edit')->with([
            
            'product'           => Product::find($id),
            'categories'        => Category::orderBy('title', 'ASC')->get(),
            'vats'              => Vat::orderBy('rate', 'ASC')->orderBy('title', 'ASC')->get(),
            'allergies'         => Allergy::get(),
            'product_allergies' => Product::find($id)->allergies()->pluck('id')->toArray(),
        ]);
    }

    /**
     * Store new product
     *
     * @param StoreProductRequest $request
     */
    public function store(StoreProductRequest $request)
    {
        $product = new Product();
        $product->fill($request->only($product->getFillable()));
        $product->save();

        $product->allergies()->sync($request->allergies);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $path = request()->file('image')->storeAs('images/products', $product->title.'_'.$product->id.'.'.$request->file('image')->getClientOriginalExtension(), 'public');
            
            $product->image = $path;
            $product->save();
        }
        
        return redirect()->route('products.index')->with('message', 'Product toegevoegd');
    }

    /**
     * Update product
     *
     * @param StoreProductRequest $request
     * @param int $id
     */
    public function update(StoreProductRequest $request, int $id)
    {
        $product = Product::find($id);
        $product->update($request->only($product->getFillable()));
        $product->allergies()->sync($request->allergies);
        $product->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $path = request()->file('image')->storeAs('images/products', $product->title.'_'.$product->id.'.'.$request->file('image')->getClientOriginalExtension(), 'public');
            
            $product->image = $path;
            $product->save();
        }
        
        return redirect()->route('products.index')->with('message', 'Product is gewijzigd');
    }

    /**
     * Delete product
     *
     * @param int $id
     */
    public function delete(int $id)
    {
        Product::find($id)->delete();

        return redirect()->route('products.index')->with('message', 'Product is verwijderd');
    }

    /**
     * Get products index table
     *
     * @param String $category
     * @param String $search
     */
    public function getProductsIndexTable(String $category, String $search)
    {
        $products = Product::search($search)
                        ->category($category)
                        ->with(['category', 'vat'])
                        ->orderBy('title', 'ASC')
                        ->get();
                    
        $html = view('admin.partials.products.index_table', compact('products'))->render();

        return $html;
    }
}
