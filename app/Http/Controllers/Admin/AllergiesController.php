<?php

namespace App\Http\Controllers\Admin;

use App\Models\Allergy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Allergies\StoreAllergyRequest;

class AllergiesController extends Controller
{
    /**
     * Show all allergy
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.allergies.index')->with('allergies', Allergy::all());
    }

    /**
     * Create new allergy
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.allergies.create');
    }

    /**
     * Edit allergy
     *
     * @param Allergy $allergy
     *
     * @return \Illuminate\View\View
     */
    public function edit(Allergy $allergy)
    {
        return view('admin.allergies.edit')->with('allergy', $allergy);
    }

    /**
     * Store new allergy
     *
     * @param StoreAllergyRequest $request
     */
    public function store(StoreAllergyRequest $request)
    {
        $allergy = new Allergy();
        $allergy->fill($request->only($allergy->getFillable()));
        $allergy->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $path = request()->file('image')->storeAs('images/allergies', $allergy->title.'_'.$allergy->id.'.'.$request->file('image')->getClientOriginalExtension(), 'public');

            $allergy->image = $path;
            $allergy->save();
        }

        return redirect()->route('allergies.index')->with('message', __('Allergie toegevoegd'));
    }

    /**
     * Update allergy
     *
     * @param StoreAllergyRequest $request
     * @param Allergy $allergy
     */
    public function update(StoreAllergyRequest $request, Allergy $allergy)
    {
        $allergy->update($request->only($allergy->getFillable()));
        $allergy->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $path = request()->file('image')->storeAs('images/allergies', $allergy->title.'_'.$allergy->id.'.'.$request->file('image')->getClientOriginalExtension(), 'public');

            $allergy->image = $path;
            $allergy->save();
        }

        return redirect()->route('allergies.edit', $allergy->id)->with('message', 'Allergie gewijzigd');
    }

    /**
     * Delete allergy
     *
     * @param Allergy $allergy
     */
    public function delete(Allergy $allergy)
    {
        $allergy->delete();

        return redirect()->route('allergies.index')->with('message', 'Allergie is verwijderd');
    }

    /**
     * Get allergy index table
     *
     * @param String $search
     */
    public function getIndexTable(String $search)
    {
        $allergies = Allergy::search($search)->get();

        $html = view('admin.partials.allergies.index_table', compact('allergies'))->render();

        return $html;
    }
}
