<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\StoreOrderRequest;
use App\Http\Requests\Orders\UpdateOrderRequest;

class OrdersController extends Controller
{
    /**
     * Show index
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.orders.index');
    }
}
