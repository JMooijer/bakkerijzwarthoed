<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Vat;
use App\Models\Allergy;
use App\Http\Controllers\Controller;

class DiscountsController extends Controller
{
    /**
     * Show all discounts
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.discounts.index')->with([
            
            'products'      => Product::with(['category', 'vat'])->whereNotNull('discount_price')->orderBy('title', 'ASC')->get(),
        ]);
    }

    /**
     * Create new discount
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.discounts.create')->with([
            
            'products'    => Category::orderBy('title', 'ASC')->get(),
        ]);
    }

    /**
     * Edit discount
     *
     * @param int $id
     * 
     * @return \Illuminate\View\View
     */
    public function edit(int $id)
    {
        return view('admin.products.edit')->with([
            
            'product'           => Product::find($id),
            'categories'        => Category::orderBy('title', 'ASC')->get(),
            'vats'              => Vat::orderBy('rate', 'ASC')->orderBy('title', 'ASC')->get(),
            'allergies'         => Allergy::get(),
            'product_allergies' => Product::find($id)->allergies()->pluck('id')->toArray(),
        ]);
    }

    /**
     * Store new discount
     *
     * @param StoreProductRequest $request
     */
    public function store(StoreProductRequest $request)
    {
        $product = new Product();
        $product->fill($request->only($product->getFillable()));
        $product->save();

        $product->allergies()->sync($request->allergies);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $path = request()->file('image')->storeAs('images/products', $product->title.'_'.$product->id.'.'.$request->file('image')->getClientOriginalExtension(), 'public');
            
            $product->image = $path;
            $product->save();
        }
        
        return redirect()->route('products.index')->with('message', 'Product toegevoegd');
    }

    /**
     * Update discount
     *
     * @param StoreProductRequest $request
     * @param int $id
     */
    public function update(StoreProductRequest $request, int $id)
    {
        $product = Product::find($id);
        $product->update($request->only($product->getFillable()));
        $product->allergies()->sync($request->allergies);
        $product->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {

            $path = request()->file('image')->storeAs('images/products', $product->title.'_'.$product->id.'.'.$request->file('image')->getClientOriginalExtension(), 'public');
            
            $product->image = $path;
            $product->save();
        }
        
        return redirect()->route('products.index')->with('message', 'Product is gewijzigd');
    }

    /**
     * Delete discount
     *
     * @param int $id
     */
    public function delete(int $id)
    {
        Product::find($id)->delete();

        return redirect()->route('products.index')->with('message', 'Product is verwijderd');
    }

    /**
     * Get discount index table
     *
     * @param String $category
     * @param String $search
     */
    public function getProductsIndexTable(String $category, String $search)
    {
        $products = Product::search($search)
                        ->category($category)
                        ->with(['category', 'vat'])
                        ->orderBy('title', 'ASC')
                        ->get();
                    
        $html = view('admin.partials.products.index_table', compact('products'))->render();

        return $html;
    }
}
