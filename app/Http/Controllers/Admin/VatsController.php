<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Vat;
use App\Http\Requests\Vats\StoreVatRequest;
use App\Http\Controllers\Controller;

class VatsController extends Controller
{
    /**
     * Show all vats
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.vats.index')->with('vats', Vat::all());
    }

    /**
     * Create new vat
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.vats.create');
    }

    /**
     * Edit vat
     *
     * @param Vat $vat
     * 
     * @return \Illuminate\View\View
     */
    public function edit(Vat $vat)
    {
        return view('admin.vats.edit')->with('vat', $vat);
    }

    /**
     * Store new vat
     *
     * @param StoreVatRequest $request
     */
    public function store(StoreVatRequest $request)
    {
        $vat = new Vat();
        $vat->fill($request->only($vat->getFillable()));

        if ($request->default) {

            Vat::query()->update(['default' => 0]);
            $vat->default = 1;
        }

        $vat->save();
        
        return redirect()->route('vats.index')->with('message', 'BTW tarief toegevoegd');
    }

    /**
     * Update vat
     *
     * @param StoreVatRequest $request
     * @param Vat $vat
     */
    public function update(StoreVatRequest $request, Vat $vat)
    {
        $vat->update($request->only($vat->getFillable()));

        if ($request->default) {

            Vat::query()->update(['default' => 0]);
            $vat->default = 1;
        } else {

            $vat->default = 0;
        }

        $vat->save();
        
        return redirect()->route('vats.index')->with('message', 'BTW tarief is gewijzigd');
    }

    /**
     * Delete vat
     *
     * @param Vat $vat
     */
    public function delete(Vat $vat)
    {
        $vat->delete();

        return redirect()->route('vats.index')->with('message', 'BTW tarief is verwijderd');
    }

    /**
     * Get vats index table
     *
     * @param String $search
     */
    public function getVatsIndexTable(String $search)
    {
        $vats = Vat::search($search)->get();
                    
        $html = view('admin.partials.vats.index_table', compact('vats'))->render();

        return $html;
    }
}
