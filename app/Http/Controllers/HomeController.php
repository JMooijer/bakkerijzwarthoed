<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show index
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('site.home.index')->with([
                'categories'    => Category::all(),
                'products'      => Product::all(),
            ]
        );
    }
}
