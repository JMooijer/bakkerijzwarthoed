<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'title'             => 'required|string|max:255',
            'category_id'       => 'nullable|string',
            'description'       => 'nullable|string|max:1200',
            'price'             => 'required|string|max:255',
            'discount_price'    => 'nullable|string|max:255',
            'vat_id'            => 'nullable|string',
            'image'             => 'nullable|file',
            'allergies'         => 'nullable',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'    => 'Titel is verplicht',
            'description.max'   => 'Beschrijving is te lang',
            'price.required'    => 'Prijs is verplicht',
        ];
    }
}
