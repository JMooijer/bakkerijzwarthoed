<?php

namespace App\Http\Requests\Allergies;

use Illuminate\Foundation\Http\FormRequest;

class StoreAllergyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'title'             => 'required|string|max:255',
            'description'       => 'nullable|string|max:1200',
            'image'             => 'nullable|file',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required'    => 'Titel is verplicht',
            'description.max'   => 'Beschrijving is te lang',
        ];
    }
}
