<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'payment_status'            => 'required|string|max:255',
            'mollie_id'                 => 'nullable|string|max:255',
            'paid_at'                   => 'nullable|string|max:255',
            'amount'                    => 'nullable|string|max:255',
            'customer_email'            => 'required|string|max:255',
            'customer_first_name'       => 'required|string|max:255',
            'customer_last_name'        => 'required|string|max:255',
            'invoice_address'           => 'required|string|max:255',
            'invoice_address_addition'  => 'nullable|string|max:255',
            'invoice_place'             => 'required|string|max:255',
            'invoice_zipcode'           => 'required|string|max:255',
            'invoice_address'           => 'nullable|string|max:255',
            'invoice_address_addition'  => 'nullable|string|max:255',
            'invoice_place'             => 'nullable|string|max:255',
            'invoice_zipcode'           => 'nullable|string|max:255',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'payment_status.required'           => 'Betaalstatus is verplicht',
            'customer_email.required'           => 'E-mailadres is verplicht',
            'customer_first_name.required'      => 'Voornaam is verplicht',
            'customer_last_name.required'       => 'Achternaam is verplicht',
            'invoice_address.required'          => 'Factuur adres is verplicht',
            'invoice_place.required'            => 'Factuur plaats is verplicht',
            'invoice_zipcode.required'          => 'Factuur postcode is verplicht',
        ];
    }
}
