<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'payment_status'    => 'required|string|max:255',
            'mollie_id'         => 'required|string|max:255',
            'paid_at'           => 'required|string|max:255',
            'amount'            => 'required|string|max:255',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

            'payment_status.required'   => 'Betaalstatus is verplicht',
            'mollie_id.required'        => 'Mollie ID is verplicht',
            'paid_at.required'          => 'E-mailadres is verplicht',
            'amount.required'           => 'Bedrag is verplicht',
        ];
    }
}
