<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;
use Gloudemans\Shoppingcart\Facades\Cart;

class HomeProducts extends Component
{
    public array $quantity = [];
    public $products;

    public function mount()
    {
        $this->products = Product::all();

        foreach ($this->products as $product) {
            $this->quantity[$product->id] = 1;
        }
    }

    public function render()
    {
        return view('livewire.home-products');
    }

    public function addToCart($product_id)
    {
        $product = Product::findOrFail($product_id);
        
        Cart::add(
            $product->id,
            $product->title,
            $this->quantity[$product_id],
            (!empty($product->discount_price) ? $product->discount_price : $product->price),
            0,
            ['image' => $product->image]
        );

        $this->emit('cart_updated');
    }
}
