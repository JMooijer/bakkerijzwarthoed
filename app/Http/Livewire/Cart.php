<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Gloudemans\Shoppingcart\Facades\Cart as ShoppingCart;

class Cart extends Component
{
    public $products;
    public $subtotal;

    protected $listeners = [
        'cart_updated' => 'render',
    ];

    /**
     * Render livewire component
     */
    public function render()
    {
        $this->subtotal     = ShoppingCart::subtotal();
        $this->products     = ShoppingCart::content();
        $cart_count         = ShoppingCart::content()->count();

        return view('livewire.cart')->with(['cart_count' => $cart_count]);
    }

    /**
     * Remove product from Cart
     */
    public function removeProduct($row_id)
    {
        ShoppingCart::remove($row_id);

        $this->render();
    }
}
