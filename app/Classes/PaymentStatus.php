<?php

namespace App\Classes;


class PaymentStatus
{
    const CANCELED      = 'canceled';
    const OPEN          = 'open';
    const PENDING       = 'pending';
    const AUTHORIZED    = 'authorized';
    const EXPIRED       = 'expired';
    const FAILED        = 'failed';
    const PAID          = 'paid';
}
