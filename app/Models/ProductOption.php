<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Association
 * @package App\Models
 *
 * @property int            id
 * @property string         title
 * @property decimal        price
 * @property null|Carbon    created_at
 * @property null|Carbon    updated_at
 */
class ProductOption extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'price',
    ];


    /*
      |-------------------------------------------------------------------------
      | Relationships
      |-------------------------------------------------------------------------
     */


    /*
      |-------------------------------------------------------------------------
      | Query scopes
      |-------------------------------------------------------------------------
     */ 


    /*
      |-------------------------------------------------------------------------
      | Accessors
      |-------------------------------------------------------------------------
     */
}
