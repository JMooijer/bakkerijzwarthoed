<?php
namespace App\Models;

use Carbon\Carbon;
use App\Models\QuestionList;
use App\Models\Question;
use App\Models\Participant;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Association
 * @package App\Models
 *
 * @property int            id
 * @property string         slug
 * @property int            category_id
 * @property string         title
 * @property string         description
 * @property decimal        price
 * @property decimal        discount_price
 * @property int            vat_id
 * @property string         image
 * @property null|Carbon    created_at
 * @property null|Carbon    updated_at
 */
class Product extends Model {

    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'category_id',
        'title',
        'description',
        'price',
        'discount_price',
        'vat_id',
    ];


    /*
      |-------------------------------------------------------------------------
      | Relationships
      |-------------------------------------------------------------------------
     */

    /**
     * Get the category of the product
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the vat of the product
     */
    public function vat()
    {
        return $this->belongsTo(Vat::class);
    }

    /**
     * The roles that belong to the user.
     */
    public function allergies()
    {
        return $this->belongsToMany(Allergy::class, 'allergy_product');
    }


    /*
      |-------------------------------------------------------------------------
      | Query scopes
      |-------------------------------------------------------------------------
     */

    /*
      |-------------------------------------------------------------------------
      | Attributes
      |-------------------------------------------------------------------------
     */

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param integer $category
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeCategory($query, $category) {

      if (blank($category) || $category == 'all') {

          return $query;
      }

      return $query->where(function ($query) use ($category) {

          $query->where('category_id', $category);
      });
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param String $search
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeSearch($query, $search) {

        if (blank($search) || $search == 'all_products') {

            return $query;
        }

        return $query->where(function ($query) use ($search) {

            $query->where('title', 'like', '%' . $search . '%')
                ->orWhere('description', 'like', '%' . $search . '%')
                ->orWhere('price', 'like', '%' . $search . '%')
                ->orWhere('discount_price', 'like', '%' . $search . '%');
        });
    }
    


    /*
      |-------------------------------------------------------------------------
      | Accessors
      |-------------------------------------------------------------------------
     */

    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Get price
     */
    public function getPrice(): string
    {
        return number_format($this->price, 2, ',', '.');
    }

    /**
     * Get discount price
     */
    public function getDiscountPrice(): string
    {
        return number_format($this->discount_price, 2, ',', '.');
    }
}
