<?php
namespace App\Models;

use Carbon\Carbon;
use App\Models\QuestionList;
use App\Models\Question;
use App\Models\Participant;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Association
 * @package App\Models
 *
 * @property int            id
 * @property string         title
 * @property string         description
 * @property string         image
 * @property null|Carbon    created_at
 * @property null|Carbon    updated_at
 */
class Allergy extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'allergies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
    ];


    /*
      |-------------------------------------------------------------------------
      | Relationships
      |-------------------------------------------------------------------------
     */

    /**
     * Get the category of the product
     */
    public function product()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * The roles that belong to the user.
     */
    public function allergies()
    {
        return $this->belongsToMany(Product::class, 'allergy_product');
    }


    /*
      |-------------------------------------------------------------------------
      | Query scopes
      |-------------------------------------------------------------------------
     */   

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param String $search
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeSearch($query, $search) {

      if (blank($search) || $search == 'all_allergies') {

          return $query;
      }

      return $query->where(function ($query) use ($search) {

          $query->where('title', 'like', '%' . $search . '%')
              ->orWhere('description', 'like', '%' . $search . '%');
      });
  }


    /*
      |-------------------------------------------------------------------------
      | Accessors
      |-------------------------------------------------------------------------
     */
}
