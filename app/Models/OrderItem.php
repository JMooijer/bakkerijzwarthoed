<?php
namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Association
 * @package App\Models
 *
 * @property int            id
 * @property int            order_id
 * @property int            product_id
 * @property int            vat_id
 * @property decimal        amount
 * @property int            quantity
 * @property null|Carbon    created_at
 * @property null|Carbon    updated_at
 */
class OrderItem extends Model {


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'product_id',
        'vat_id',
        'amount',
        'quantity',
    ];


    /*
      |-------------------------------------------------------------------------
      | Relationships
      |-------------------------------------------------------------------------
     */

    /**
     * Get the order of the order item
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }


    /*
      |-------------------------------------------------------------------------
      | Query scopes
      |-------------------------------------------------------------------------
     */    


    /*
      |-------------------------------------------------------------------------
      | Accessors
      |-------------------------------------------------------------------------
     */
}
