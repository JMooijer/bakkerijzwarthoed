<?php
namespace App\Models;

use Carbon\Carbon;
use App\Models\QuestionList;
use App\Models\Question;
use App\Models\Participant;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Association
 * @package App\Models
 *
 * @property int            id
 * @property string         slug
 * @property string         title
 * @property string         description
 * @property string         image
 * @property null|Carbon    created_at
 * @property null|Carbon    updated_at
 */
class Category extends Model {

  use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'title',
        'description',
        'image',
    ];


    /*
      |-------------------------------------------------------------------------
      | Relationships
      |-------------------------------------------------------------------------
     */

    /**
     * Get the product
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }


    /*
      |-------------------------------------------------------------------------
      | Query scopes
      |-------------------------------------------------------------------------
     */

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param String $search
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeSearch($query, $search) {

      if (blank($search) || $search == 'all_categories') {

          return $query;
      }

      return $query->where(function ($query) use ($search) {

          $query->where('title', 'like', '%' . $search . '%')
              ->orWhere('description', 'like', '%' . $search . '%');
      });
  }


    /*
      |-------------------------------------------------------------------------
      | Accessors
      |-------------------------------------------------------------------------
     */

     /**
      * return slug
      */
    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
