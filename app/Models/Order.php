<?php
namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Association
 * @package App\Models
 *
 * @property int            id
 * @property string         payment_status
 * @property string         mollie_id
 * @property string         paid_at
 * @property decimal        amount
 * @property string         customer_email
 * @property string         customer_first_name
 * @property string         customer_last_name
 * @property string         invoice_address
 * @property string         invoice_address_addition
 * @property string         invoice_place
 * @property string         invoice_zipcode
 * @property string         shipping_address
 * @property string         shipping_address_addition
 * @property string         shipping_place
 * @property string         shipping_zipcode
 * @property null|Carbon    created_at
 * @property null|Carbon    updated_at
 */
class Order extends Model {


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'payment_status',
        'customer_email',
        'customer_first_name',
        'customer_last_name',
        'invoice_address',
        'invoice_address_addition',
        'invoice_place',
        'invoice_zipcode',
        'shipping_address',
        'shipping_address_addition',
        'shipping_place',
        'shipping_zipcode',
    ];


    /*
      |-------------------------------------------------------------------------
      | Relationships
      |-------------------------------------------------------------------------
     */

    /**
     * Get the order items of the order
     */
    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }


    /*
      |-------------------------------------------------------------------------
      | Query scopes
      |-------------------------------------------------------------------------
     */    


    /*
      |-------------------------------------------------------------------------
      | Accessors
      |-------------------------------------------------------------------------
     */
}
