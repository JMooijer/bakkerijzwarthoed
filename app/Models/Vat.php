<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Association
 * @package App\Models
 *
 * @property int            id
 * @property string         title
 * @property decimal        rate
 * @property boolean        default
 * @property null|Carbon    created_at
 * @property null|Carbon    updated_at
 */
class Vat extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vats';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'rate',
    ];


    /*
      |-------------------------------------------------------------------------
      | Relationships
      |-------------------------------------------------------------------------
     */

    /**
     * Get the products
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }


    /*
      |-------------------------------------------------------------------------
      | Query scopes
      |-------------------------------------------------------------------------
     */ 

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param String $search
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeSearch($query, $search) {

        if (blank($search) || $search == 'all_vats') {

            return $query;
        }

        return $query->where(function ($query) use ($search) {

            $query->where('title', 'like', '%' . $search . '%')
                ->orWhere('rate', 'like', '%' . $search . '%');
        });
    }


    /*
      |-------------------------------------------------------------------------
      | Accessors
      |-------------------------------------------------------------------------
     */
}
