require('./bootstrap');

require('alpinejs');

import Vue from 'vue';

// Product options
Vue.component('product-options', require('./components/Products/Options.vue').default);
Vue.component('product-variations', require('./components/Products/Variations.vue').default);


const app = new Vue({
    el: '#app',
});


$(document).ready(function () {

    setTimeout(function () {
        $(".alert").alert('close');
    }, 2000);

    $("#cart-contents").click(function (e) {
        e.stopPropagation();
    })

    $(".navbar-toggler").on("click", function () {
        $("#navbarNavDropdown").css("width", "100%");
    });

    $(".navbar-close").on("click", function () {
        $("#navbarNavDropdown").css("width", "0");
    });

    if ($(".sidebar").length) {
        let arrow = document.querySelectorAll(".fas.fa-chevron-down");
        for (var i = 0; i < arrow.length; i++) {
            arrow[i].addEventListener("click", (e) => {
                let arrowParent = e.target.parentElement.parentElement;//selecting main parent of arrow
                arrowParent.classList.toggle("showMenu");
            });
        }

        let sidebar = document.querySelector(".sidebar");
        let sidebarBtn = document.querySelector(".fas.fa-bars");

        sidebarBtn.addEventListener("click", () => {
            sidebar.classList.toggle("closed");
        });

        let sidebarBtn2 = document.querySelector(".fas.fa-times");

        sidebarBtn2.addEventListener("click", () => {
            sidebar.classList.toggle("closed");
        });

        $("#product-category").on("change", function () {

            searchProducts();
        });

        $("#product-search").on("keyup", function () {

            searchProducts();
        });

        $("#category-search").on("keyup", function () {

            var search = $("#category-search").val();

            if (search == null || search == "") {

                search = 'all_categories';
            }

            $.ajax({
                url: "/categories/get-categories-index-table/" + search,
                type: "GET",
                success: function success(result) {
                    $(".table-responsive").html(result);
                },
                error: function error(_error) {
                    console.log(_error);
                }
            });
        });

        $("#vat-search").on("keyup", function () {

            var search = $("#vat-search").val();

            if (search == null || search == "") {

                search = 'all_vats';
            }

            $.ajax({
                url: "/vats/get-vats-index-table/" + search,
                type: "GET",
                success: function success(result) {
                    $(".table-responsive").html(result);
                },
                error: function error(_error) {
                    console.log(_error);
                }
            });
        });

        $("#allergy-search").on("keyup", function () {

            var search = $("#allergy-search").val();

            if (search == null || search == "") {

                search = 'all_allergies';
            }

            $.ajax({
                url: "/allergies/get-index-table/" + search,
                type: "GET",
                success: function success(result) {
                    $(".table-responsive").html(result);
                },
                error: function error(_error) {
                    console.log(_error);
                }
            });
        });

        $("#allergies-select").on("change", function () {

            console.log($this.val());
        });

        function searchProducts() {

            var category = $("#product-category").val();
            var search = $("#product-search").val();

            if (search == null || search == "") {

                search = 'all_products';
            }

            $.ajax({
                url: "/products/get-products-index-table/" + category + "/" + search,
                type: "GET",
                success: function success(result) {
                    $(".table-responsive").html(result);
                },
                error: function error(_error) {
                    console.log(_error);
                }
            });
        }
    }
});
