@extends('layouts.dashboard')

@section('content')

<div class="container-fluid dashboard pb-3">
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats h-100">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{__('Bestellingen van deze week')}}</h5>
                            <span class="h2 font-weight-bold mb-0">17</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                <i class="ni ni-active-40"></i>
                            </div>
                        </div>
                    </div>
                    <p class="mt-3 mb-0 text-sm">
                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span><br/>
                        <span class="text-nowrap">{{__('Ten opzichte van vorige week')}}</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats h-100">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{__('Bestellingen van deze maand')}}</h5>
                            <span class="h2 font-weight-bold mb-0">89</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                <i class="ni ni-chart-pie-35"></i>
                            </div>
                        </div>
                    </div>
                    <p class="mt-3 mb-0 text-sm">
                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span><br/>
                        <span class="text-nowrap">{{__('Ten opzichte van vorige week')}}</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats h-100">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{__('Bezoekers deze week')}}</h5>
                            <span class="h2 font-weight-bold mb-0">924</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                <i class="ni ni-money-coins"></i>
                            </div>
                        </div>
                    </div>
                    <p class="mt-3 mb-0 text-sm">
                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span><br/>
                        <span class="text-nowrap">{{__('Ten opzichte van vorige week')}}</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card card-stats h-100">
                <!-- Card body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{__('Totaal aantal bezoekers')}}</h5>
                            <span class="h2 font-weight-bold mb-0">1200</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                <i class="ni ni-chart-bar-32"></i>
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4 graphs">
        <div class="col-xs-12 col-sm-6">
            <div class="card h-100">
                <div class="card-body">
                    <canvas id="myChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="card h-100">
                <div class="card-body">
                    <div>
                        <b>{{__('Deze week')}}</b><br/>
                        € 785,50
                    </div>
                    <div>
                        <b>{{__('Deze maand')}}</b><br/>
                        € 2350,00
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag'],
            datasets: [
                {
                    label: 'Bestellingen',
                    data: [12, 19, 3, 5, 12, 16, 21],                 
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)'
                    ],
                    borderWidth: 1
                },
                {
                    label: 'Omzet',
                    data: [120, 87, 140, 158, 207, 270, 300],
                    backgroundColor: [
                        '#3490dc'
                    ],
                    borderColor: [
                        '#3490dc'
                    ],
                    borderWidth: 1
                }
            ]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            },
            maintainAspectRatio: false,
        }
    });
    </script>
@endsection