<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Google Tag Manager -->
    {{-- <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WFLLVZ');
    </script> --}}
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}" defer></script>
    <script src="{{ asset('js/ckeditor/adapters/jquery.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
</head>

<body class="dashboard">
    <div id="app">
        <div class="sidebar closed">
            <i class="fas fa-times"></i>
            <div class="logo-details pt-4">
                <i class='bx bxl-c-plus-plus'></i>
                <span class="logo_name">Bakkerij zwarthoed</span>
            </div>
            <ul class="nav-links">
                <li>
                    <a href="{{ route('dashboard') }}">
                        <i class="fas fa-tachometer-alt"></i>
                        <span class="link_name">{{__('Dashboard')}}</span>
                    </a>
                </li>
                <li>
                    <div class="icon-link">
                        <a href="{{ route('products.index') }}">
                            <i class="fas fa-box"></i>
                            <span class="link_name">{{__('Producten')}}</span>
                        </a>
                        <i class='fas fa-chevron-down'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a class="link_name" href="#">{{__('Producten')}}</a></li>
                        <li><a href="{{ route('products.index') }}">{{__('Alle producten')}}</a></li>
                        <li><a href="{{ route('products.create') }}">{{__('Product toevoegen')}}</a></li>
                    </ul>
                </li>
                <li>
                    <div class="icon-link">
                        <a href="{{ route('categories.index') }}">
                            <i class="fas fa-th-large"></i>
                            <span class="link_name">{{__('Categorieën')}}</span>
                        </a>
                        <i class='fas fa-chevron-down'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a class="link_name" href="#">{{__('Categorieën')}}</a></li>
                        <li><a href="{{ route('categories.index') }}">{{__('Alle categorieën')}}</a></li>
                        <li><a href="{{ route('categories.create') }}">{{__('Categorie toevoegen')}}</a></li>
                    </ul>
                </li>
                <li>
                    <div class="icon-link">
                        <a href="{{ route('categories.index') }}">
                            <i class="fas fa-tags"></i>
                            <span class="link_name">{{__('Aanbiedingen')}}</span>
                        </a>
                        <i class='fas fa-chevron-down'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a class="link_name" href="#">{{__('Aanbiedingen')}}</a></li>
                        <li><a href="{{ route('categories.index') }}">{{__('Alle aanbiedingen')}}</a></li>
                        <li><a href="{{ route('categories.create') }}">{{__('Aanbieding toevoegen')}}</a></li>
                    </ul>
                </li>
                <li>
                    <div class="icon-link">
                        <a href="{{ route('allergies.index') }}">
                            <i class="fas fa-allergies"></i>
                            <span class="link_name">{{__('Allergieën')}}</span>
                        </a>
                        <i class='fas fa-chevron-down'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a class="link_name" href="#">{{__('Allergieën')}}</a></li>
                        <li><a href="{{ route('allergies.index') }}">{{__('Alle allergieën')}}</a></li>
                        <li><a href="{{ route('allergies.create') }}">{{__('Allergie toevoegen')}}</a></li>
                    </ul>
                </li>
                <li>
                    <div class="icon-link">
                        <a href="{{ route('orders.index') }}">
                            <i class="fas fa-euro-sign"></i>
                            <span class="link_name">{{__('Bestellingen')}}</span>
                        </a>
                        <i class='fas fa-chevron-down'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a class="link_name" href="#">{{__('Bestellingen')}}</a></li>
                        <li><a href="{{ route('orders.index') }}">{{__('Alle bestellingen')}}</a></li>
                    </ul>
                </li>
                <li>
                    <div class="icon-link">
                        <a href="{{ route('vats.index') }}">
                            <i class="fas fa-percent"></i>
                            <span class="link_name">{{__('BTW')}}</span>
                        </a>
                        <i class='fas fa-chevron-down'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a class="link_name" href="#">{{__('BTW')}}</a></li>
                        <li><a href="{{ route('vats.index') }}">{{__('BTW tarieven')}}</a></li>
                        <li><a href="{{ route('vats.create') }}">{{__('BTW tarief toevoegen')}}</a></li>
                    </ul>
                </li>
                <li>
                    <div class="icon-link">
                        <a href="{{ route('users.index') }}">
                            <i class="fas fa-users"></i>
                            <span class="link_name">{{__('Gebruikers')}}</span>
                        </a>
                        <i class='fas fa-chevron-down'></i>
                    </div>
                    <ul class="sub-menu">
                        <li><a class="link_name" href="#">{{__('Gebruikers')}}</a></li>
                        <li><a href="{{ route('users.index') }}">{{__('Alle gebruikers')}}</a></li>
                        <li><a href="{{ route('users.create') }}">{{__('Gebruiker toevoegen')}}</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <section class="home-section">
            <div class="home-content">
                <i class="fas fa-bars"></i>
                <span class="text pl-1">
                    <img src="{{asset('images/logo.png')}}" alt="Bakkerij zwarthoed" class="img-fluid" style="max-height: 60px;"/>
                </span>
            </div>
            @yield('content')
        </section>
    </div>
</body>

</html>
