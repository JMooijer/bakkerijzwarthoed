@extends('layouts.auth')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="wrapper fadeInDown">
                    <div id="formContent">

                        <!-- Login Form -->
                        <form action="{{ route('login') }}" method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="email">{{__('E-mailadres')}}</label>
                                <input type="email" name="email" class="form-control fadeIn first"/>
                            </div>

                            <div class="form-group">
                                <label for="password">{{__('Wachtwoord')}}</label>
                                <input type="password" name="password" class="form-control fadeIn second">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary fadeIn third">{{__('Login')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
