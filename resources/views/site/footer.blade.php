<section id="footer-categories">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-4 pl-sm-0 mb-3 mb-sm-0">
                <div class="category" style="background-image: url(/images/footer/gelegenheden.jpg); background-size: cover;">
                    <div class="category-inner">
                        <h2>Gelegenheden</h2>
                        <a href="#" class="btn btn-transparant">Bekijk alle gelegenheden</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 mb-3 mb-sm-0">
                <div class="category" style="background-image: url(/images/footer/onze-specialiteiten.jpg); background-size: cover;">
                    <div class="category-inner">
                        <h2>Onze specialiteiten</h2>
                        <a href="#" class="btn btn-transparant">Onze specialiteiten</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 pr-sm-0">
                <div class="category" style="background-image: url(/images/footer/openingstijden.jpg); background-size: cover;">
                    <div class="category-inner">
                        <h2>Openingstijden</h2>
                        <a href="#" class="btn btn-transparant">Openingstijden</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="list-unstyled pages">
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="#">Wie zijn wij?</a>
                    </li>
                    <li>
                        <a href="#">Assortiment</a>
                    </li>
                    <li>
                        <a href="#">Onze specialiteiten</a>
                    </li>
                    <li>
                        <a href="#">Gelegenheden</a>
                    </li>
                    <li>
                        <a href="#">Openingstijden</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
                <ul class="list-unstyled extra-pages">
                    <li>
                        {{date('Y')}} © Banketbakkerij Zwarthoed
                    </li>
                    <li>
                        <a href="#">Algemene voorwaarden</a>
                    </li>
                    <li>
                        <a href="#">Privacy policy</a>
                    </li>
                    <li>
                        Website: Johnny Mooijer & Stanley Kroon
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>