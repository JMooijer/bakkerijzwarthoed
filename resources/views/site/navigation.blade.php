<nav class="navbar navbar-expand-lg navbar-light py-3">
    <div class="container-xl position-relative">
        {{-- <a class="navbar-brand" href="#">
            <img src="/images/logo.png" alt="Bakkerij Zwarthoed" title="Bakkerij zwarthoed" class="img-fluid">
        </a> --}}
        <button class="navbar-toggler" type="button" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse justify-content-end" id="navbarNavDropdown">
            <a href="javascript:void(0)" class="navbar-close" onclick="closeNav()">&times;</a>
            <ul class="navbar-nav">
                @foreach ($categories as $category)
                    <li class="nav-item">
                        <a class="nav-link" href="{{$category->slug}}">{{$category->title}}</a>
                    </li>
                @endforeach
                <li class="nav-item">
                    <a href="#" class="nav-link">Gelegenheden</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fas fa-phone-alt"></i> 0299-363270
                    </a>
                </li>
                <li class="nav-item">
                    @livewire('cart')
                </li>
                <li class="nav-item">               
                    <a href="#" class="btn btn-primary">Nu bestellen</a>            
                </li>
            </ul>
        </div>
    </div>
</nav>