@extends('layouts.site')

@section('content')
    <section id="category-detail">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div class="categories-overview">
                        <ul class="list-unstyled">
                            @foreach ($categories as $cat)
                                <li {{$cat->id == $category->id ? 'class=active' : ''}}>
                                    <a href="{{$cat->slug}}">{{$cat->title}} ({{count($cat->products)}})</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="category-information">
                        <h1>{{$category->title}}</h1>
                        <img src="{{$category->image}}" alt="{{$category->title}}" class="img-fluid category-image float-left pr-5"/>
                        {!!$category->description!!}
                    </div>
                    <hr/>
                    <div class="row category-products mt-4">
                        @foreach ($category->products as $product)
                            <div class="col-md-4 category-product">
                                <img src="{{$product->image}}" alt="{{$product->title}}" class="img-fluid"/>
                                <div class="category-product-information">
                                    <h3>{{$product->title}}</h3>
                                    {!!$product->description!!}
                                </div>                               
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection