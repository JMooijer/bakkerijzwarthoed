@extends('layouts.site')

@section('content')
    <section id="intro">
        <div class="container-xl xl-container position-relative">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <h1 class="home-h1">
                        Banketbakkerij<br/>
                        Zwarthoed
                    </h1>
                    <p>
                        Bent u op zoek naar het lekkerste gebak, koek of chocolade? Dan bent u bij ons aan het juiste adres!
                    </p>
                    <p>
                        Onze altijd verse en smakelijke producten zijn via deze website nu ook online verkrijgbaar.
                    </p>
                    <p>
                        Staat uw gewenste product niet online of heeft u vragen/specifieke wensen? Neem dan contact met ons op. Wij helpen u graag!
                    </p>
                    <a href="#" class="btn btn-secondary text-center d-block mb-3 max-width-60">Bekijk alle gelegenheden</a>
                    <a href="#" class="btn btn-primary text-center d-block max-width-60">Bekijk ons assortiment</a>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-8 background-image">
                    
                </div>
            </div>
            <div class="bg-dark-blue"></div>
        </div>
    </section>
    <section id="information">
        <div class="container-xl xl-container">
            <div class="row">
                <div class="d-none d-md-block col-md-4 col-xl-4 pr-0">
                    <div class="img-wrapper d-inline-block" style="background-image: url(/images/home/catalog.jpg); background-size: cover; height: 100%; width: 100%;"></div>             
                </div>
                <div class="col-sm-6 col-md-4 col-xl-3 pl-md -0">
                    <div class="bg-light-grey">
                        <div class="content d-block d-sm-inline-block pl-sm-4 position-relative">
                            <h2 class="mt-3">Assortiment</h2>
                            <ul class="list-unstyled">
                                <li>
                                    <a href="#">Gebak</a>
                                </li>
                                <li>
                                    <a href="#">Taart</a>
                                </li>
                                <li>
                                    <a href="#">Petit fours</a>
                                </li>
                                <li>
                                    <a href="#">Koek</a>
                                </li>
                                <li>
                                    <a href="#">Bonbons</a>
                                </li>
                            </ul>
                            <a href="#" class="primary-link">Bekijk het overzicht</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-3 bg-brown">
                    <div class="opening-hours position-relative">
                        <h2>Openingstijden</h2>
                        <ul class="list-unstyled text-right">
                            <li><span class="float-left">Maandag</span>09:00 - 18:00 uur</li>
                            <li><span class="float-left">Dinsdag</span>09:00 - 18:00 uur</li>
                            <li><span class="float-left">Woensdag</span>09:00 - 18:00 uur</li>
                            <li class="active"><span class="float-left">Donderdag</span>09:00 - 18:00 uur</li>
                            <li><span class="float-left">Vrijdag</span>09:00 - 18:00 uur</li>
                            <li><span class="float-left">Zaterdag</span>09:00 - 18:00 uur</li>
                            <li><span class="float-left">Zondag</span>09:00 - 18:00 uur</li>
                        </ul>
                        <a href="#">Bekijk het overzicht</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @livewire('home-products')
@endsection