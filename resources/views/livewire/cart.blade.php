<div>
    <a href="#cart-contents" class="nav-link shopping-cart-link dropdown-toggle" data-toggle="collapse" role="button" aria-expanded="false">
        <i class="fas fa-shopping-cart"></i>({{$cart_count}})
    </a>
    @if (count($products))
        @php
            $i = 0;
        @endphp
        <ul id="cart-contents" class="list-unstyled collapse" role="menu">
            @foreach ($products as $product)
                @php
                    $i++;

                    if ($i > 5) {
                        continue;
                    }
                @endphp
                <li>
                    <span class="item py-3">
                        <span class="item-left">
                            <div class="img-wrapper" style="background-image: url('{{$product->options['image']}}');"></div>
                            <span class="item-info">
                                <span>{{$product->name}}</span>
                                <span>€ {{number_format($product->price * $product->qty, 2, ',', '.')}}</span>                            
                            </span>
                        </span>
                        <span class="item-right">
                            <span class="pull-right mr-2 pt-2 align-middle">{{$product->qty}}</span>
                            <button wire:click="removeProduct('{{$product->rowId}}')" class="btn btn-xs btn-danger pull-right align-middle">x</button>
                        </span>
                    </span>
                </li>
            @endforeach
            <li>
                <span class="item py-3" style="border-top: 1px solid rgba(0, 0, 0, 0.1);">
                    <span class="item-left">
                        <span class="item-info">
                            <span>Totaal</span>                    
                        </span>
                    </span>
                    <span class="item-right">
                        <span class="d-block">€ {{number_format($subtotal, 2, ',', '.')}}</span>  
                    </span>
                </span>
            </li>
            <li>
                <span class="item py-3" style="border-top: 1px solid rgba(0, 0, 0, 0.1);">
                    <span class="item-left">
                        <span class="item-info">
                                                
                        </span>
                    </span>
                    <span class="item-right">
                        <a href="#" class="btn btn-secondary">Afrekenen</a> 
                        <a href="#" class="btn btn-primary">Winkelwagen</a>
                    </span>
                </span>
            </li>
        </ul>
    @endif
</div>