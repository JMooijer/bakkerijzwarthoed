<section id="sales">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12">
                <h2 class="text-center">Deze week in de reclame</h2>
            </div>
        </div>
        <div class="row mt-3">
            @foreach ($products as $product)
                <div class="col-xs-12 col-sm-6 col-md-3 mb-5">
                    <div class="product">
                        <div class="product-image-wrapper position-relative">
                            <span class="reduced-price">- € {{number_format($product->price - $product->discount_price, 2, ',', '.')}}</span>
                            <img src="{{$product->image}}" alt="Aardbei rondje" class="img-fluid"/>
                        </div>
                        <div class="product-information mt-3 position-relative">
                            <h3>{{$product->title}}</h3>
                            @if ($product->discount_price)
                                <div class="previous-price">Van <span class="text-decoration-line-through">€ {{$product->getPrice()}}</span></div>
                                <div class="action-price">Voor € {{$product->getDiscountPrice()}}</div>
                            @else
                                <div class="price">€ {{$product->getPrice()}}</div>
                            @endif
                            
                            <form wire:submit.prevent="addToCart({{$product->id}})" action="{{route('cart.store')}}" class="form-inline" method="POST">
                                @csrf
                                <div class="input-group mb-2">
                                    <input wire:model="quantity.{{$product->id}}" type="number" value="1" class="form-control mr-3">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fas fa-shopping-cart position-relative"></i>
                                    </button>
                                </div>                                               
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
