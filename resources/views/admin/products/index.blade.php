@extends('layouts.dashboard')

@section('content')

    <div class="container mt-5 pb-3">

        @if (session('message'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{session('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-4">
                <a href="{{route('dashboard')}}"><i class="fas fa-arrow-left"></i> {{__('Terug')}}</a>
            </div>
            <div class="col-8 mb-3 text-right">
                <a href="{{route('products.create')}}" class="btn btn-primary mb-3"><i class="fas fa-plus"></i> {{__('Product toevoegen')}}</a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label for="product-search">{{__('Zoeken')}}</label>
                                    <input type="text" name="product-search" class="form-control" id="product-search"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label for="product-category">{{__('Categorie')}}</label>
                                    <select name="product-category" id="product-category" class="form-control">
                                        <option value="all">{{__('Alle categorieën')}}</option>
                                        @foreach ($categories as $category)

                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h2 class="card-title">{{__('Producten')}}</h2>
                    </div>
                    <div class="card-body">
                        @include('admin.partials.products.index_table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
