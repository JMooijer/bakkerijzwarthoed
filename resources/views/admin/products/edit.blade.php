@extends('layouts.dashboard')

@section('content')

    <div class="container mt-5 pb-3">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <a href="{{route('products.index')}}"><i class="fas fa-arrow-left"></i> Terug</a>
                <div class="card mt-3">
                    <div class="card-body">
                        <div class="card-title">
                            <h2>{{$product->title}}</h2>
                        </div>
                        <hr/>
                        <form action="{{route('products.update', $product->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="title">{{__('Titel')}}</label>
                                <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{$product->title}}"/>

                                @error('title')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="category_id">{{__('Categorie')}}</label>
                                <select name="category_id" id="category_id" class="form-control">
                                    <option value="">--{{__('Kies een categorie')}}</option>
                                    @foreach ($categories as $category)

                                        <option value="{{$category->id}}" {{$product->category_id == $category->id ? 'selected="selected"' : ''}}>{{$category->title}}</option>
                                    @endforeach
                                </select>

                                @error('category_id')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <label for="price">{{__('Prijs')}}</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="price">€</span>
                                </div>
                                <input type="number" name="price" min="0" step=".05" class="form-control @error('price') is-invalid @enderror" value="{{$product->price}}" aria-describedby="price"/>

                                @error('price')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <label for="discount_price">{{__('Aanbiedings prijs')}}</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="discount_price">€</span>
                                </div>
                                <input type="number" name="discount_price" min="0" step=".05" class="form-control @error('discount_price') is-invalid @enderror" value="{{$product->discount_price}}" aria-describedby="discount_price"/>

                                @error('price')
                                <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="vat_id">{{__('BTW tarief')}}</label>
                                <select name="vat_id" id="vat_id" class="form-control">
                                    <option value="">--{{__('Kies BTW')}}</option>
                                    @foreach ($vats as $vat)

                                        <option value="{{$vat->id}}" {{$product->vat_id == $vat->id ? 'selected="selected"' : ''}}>{{$vat->title}}: {{$vat->rate}}%</option>
                                    @endforeach
                                </select>

                                @error('vat_id')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="allergies">{{__('Allergieën')}}</label><br/>
                                <select class="selectpicker" multiple data-none-selected-text="{{__('Geen allergieën')}}" name="allergies[]">
                                    @foreach ($allergies as $allergy)

                                        <option value="{{$allergy->id}}" @if (in_array($allergy->id, $product_allergies)) selected="selected" @endif>{{$allergy->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="description">{{__('Beschrijving')}}</label>
                                <textarea name="description" cols="30" rows="10" class="form-control ckeditor @error('description') is-invalid @enderror">{!!$product->description!!}</textarea>

                                @error('description')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="image">{{__('Afbeelding')}}</label>
                                <input type="file" name="image" class="form-control-file @error('description') is-invalid @enderror" id="image">

                                @if (!empty($product->image))

                                    <img src="/{{$product->image}}" alt="{{$product->title}}" class="img-fluid mt-3"/>
                                @endif

                                @error('image')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">{{__('Opslaan')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="card product-options">
                    <div class="card-body">
                        <div class="card-title">
                            <h2>{{__('Opties')}}</h2>
                        </div>
                        <hr/>
                        <product-options></product-options>
                    </div>
                </div>
                <div class="card product-variations">
                    <div class="card-body">
                        <div class="card-title">
                            <h2>{{__('Variaties')}}</h2>
                        </div>
                        <hr/>
                        <product-variations></product-variations>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
