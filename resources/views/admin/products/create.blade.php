@extends('layouts.dashboard')

@section('content')

    <div class="container mt-5 pb-3">
        <div class="row">
            <div class="col-12">
                <a href="{{route('products.index')}}"><i class="fas fa-arrow-left"></i> {{__('Terug')}}</a>
                <div class="card mt-3">
                    <div class="card-body">
                        <div class="card-title">
                            <h2>{{__('Product toevoegen')}}</h2>
                        </div>
                        <hr/>
                        <form action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
        
                            <div class="form-group">
                                <label for="title">{{__('Titel')}}</label>
                                <input type="text" name="title" class="form-control @error('title') is-invalid @enderror"/>

                                @error('title')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="category_id">{{__('Categorie')}}</label>
                                <select name="category_id" id="category_id" class="form-control selectpicker" data-none-selected-text="">
                                    <option value=""></option>
                                    @foreach ($categories as $category)
                                        
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>

                                @error('category_id')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="price">{{__('Prijs')}}</label>
                                <input type="number" name="price" min="0" step=".05" class="form-control @error('price') is-invalid @enderror"/>

                                @error('price')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="discount_price">{{__('Aanbiedings prijs')}}</label>
                                <input type="number" name="discount_price" min="0" step=".05" class="form-control @error('discount_price') is-invalid @enderror"/>

                                @error('discount_price')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="vat_id">{{__('BTW tarief')}}</label>
                                <select name="vat_id" id="vat_id" class="form-control selectpicker" data-none-selected-text="">
                                    @foreach ($vats as $vat)
                                        
                                        <option value="{{$vat->id}}" {{$vat->default ? 'selected="selected"' : ''}}>{{$vat->title}}: {{$vat->rate}}%</option>
                                    @endforeach
                                </select>

                                @error('vat_id')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="allergies">{{__('Allergieën')}}</label><br/>
                                <select class="selectpicker" multiple data-none-selected-text="{{__('Geen allergieën')}}" id="allergies-select" name="allergies[]">
                                    @foreach ($allergies as $allergy)
                                        
                                        <option value="{{$allergy->id}}">{{$allergy->title}}</option>
                                    @endforeach
                                </select>
                            </div>
        
                            <div class="form-group">
                                <label for="description">{{__('Beschrijving')}}</label>
                                <textarea name="description" cols="30" rows="10" class="form-control ckeditor @error('description') is-invalid @enderror"></textarea>

                                @error('description')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="image">{{__('Afbeelding')}}</label>
                                <input type="file" name="image" class="form-control-file @error('description') is-invalid @enderror" id="image">

                                @error('image')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>
        
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">{{__('Opslaan')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection