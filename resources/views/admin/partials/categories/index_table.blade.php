<table class="table categories-table">
    <thead>
        <tr>
            <th></th>
            <th>{{__('Titel')}}</th>
            <th>{{__('Beschrijving')}}</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($categories as $category)

            <tr>
                <td>
                    @if (!empty($category->image))

                        <img src="/{{ $category->image }}" alt="{{$category->title}}" class="img-fluid"/>
                    @endif
                </td>
                <td>
                    <a href="{{route('categories.edit', $category->id,)}}">{{$category->title}}</a>
                </td>
                <td>{!!$category->description!!}</td>
                <td>
                    <a href="#" type="button" data-toggle="modal" data-target="#delete-category-modal-{{$category->id}}" class="text-danger"><i class="fas fa-trash"></i></a>

                    <div id="delete-category-modal-{{$category->id}}" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 id="exampleModalLabel" class="modal-title">{{__('Categorie verwijderen')}}?</h5>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        {{__('Weet u zeker dat u')}} <b>{{$category->title}}</b> {{__('wilt verwijderen')}}?
                                    </p>
                                    @if (!empty($category->image))

                                        <img src="/{{$category->image}}" alt="{{$category->title}}" class="img-fluid">
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-secondary">{{__('Annuleren')}}</button>
                                    <a href="{{route('categories.delete', $category->id)}}" class="btn btn-danger">{{__('Verwijderen')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
