<table class="table allergies-table">
    <thead>
        <tr>
            <th></th>
            <th>{{__('Titel')}}</th>
            <th>{{__('Beschrijving')}}</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($allergies as $allergy)

            <tr>
                <td>
                    @if (!empty($allergy->image))

                        <img src="/{{ $allergy->image }}" alt="{{$allergy->title}}" class="img-fluid"/>
                    @endif
                </td>
                <td>
                    <a href="{{route('allergies.edit', $allergy->id,)}}">{{$allergy->title}}</a>
                </td>
                <td>{!!$allergy->description!!}</td>
                <td>
                    <a href="#" type="button" data-toggle="modal" data-target="#delete-allergy-modal-{{$allergy->id}}" class="text-danger"><i class="fas fa-trash"></i></a>

                    <div id="delete-allergy-modal-{{$allergy->id}}" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 id="exampleModalLabel" class="modal-title">{{__('Allergie verwijderen')}}?</h5>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        {{__('Weet u zeker dat u')}} <b>{{$allergy->title}}</b> {{__('wilt verwijderen')}}?
                                    </p>
                                    @if (!empty($allergy->image))

                                        <img src="/{{$allergy->image}}" alt="{{$allergy->title}}" class="img-fluid">
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-secondary">{{__('Annuleren')}}</button>
                                    <a href="{{route('allergies.delete', $allergy->id)}}" class="btn btn-danger">{{__('Verwijderen')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
