<table class="table allergies-table">
    <thead>
    <tr>
        <th>{{__('Naam')}}</th>
        <th>{{__('E-mailadres')}}</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach ($users as $user)

        <tr>
            <td class="text-left">
                <a href="{{route('users.edit', $user->id,)}}">{{$user->name}}</a>
            </td>
            <td>{{$user->email}}</td>
            <td>
                <a href="#" type="button" data-toggle="modal" data-target="#delete-user-modal-{{$user->id}}" class="text-danger"><i class="fas fa-trash"></i></a>

                <div id="delete-user-modal-{{$user->id}}" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
                    <div role="document" class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 id="exampleModalLabel" class="modal-title">{{__('Gebruiker verwijderen')}}?</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>
                                    {{__('Weet u zeker dat u')}} <b>{{$user->name}}</b> {{__('wilt verwijderen')}}?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">{{__('Annuleren')}}</button>
                                <a href="{{route('users.delete', $user->id)}}" class="btn btn-danger">{{__('Verwijderen')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
