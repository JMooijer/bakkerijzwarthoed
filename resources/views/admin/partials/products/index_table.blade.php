<div class="table-responsive">
    <table class="table products-table">
        <thead>
            <tr>
                <th></th>
                <th>{{__('Titel')}}</th>
                <th>{{__('Categorie')}}</th>
                <th>{{__('Allergieën')}}</th>
                <th>{{__('beschrijving')}}</th>
                <th>{{__('Prijs')}}</th>
                <th>{{__('Aanbiedings prijs')}}</th>
                <th>{{__('BTW')}}</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)

                <tr>
                    <td>
                        @if (!empty($product->image))

                            <img src="/{{ $product->image }}" alt="{{$product->title}}" class="img-fluid"/>
                        @endif
                    </td>
                    <td>
                        <a href="{{route('products.edit', $product->id,)}}">{{$product->title}}</a>
                    </td>
                    <td>{{$product->category ? $product->category->title : ''}}</td>
                    <td>
                        @foreach ($product->allergies as $allergy)

                            @if (!$loop->first)

                                <br/>
                            @endif

                            {{$allergy->title}}
                        @endforeach
                    </td>
                    <td>{!!$product->description!!}</td>
                    <td>€ {{number_format($product->price, 2, ',', '.')}}</td>
                    <td>{{$product->discount_price ? '€ '.number_format($product->discount_price, 2, ',', '.') : '-'}}</td>
                    <td>{{$product->vat_id ? $product->vat->rate.'%' : '-'}}</td>
                    <td>
                        <a href="#" type="button" data-toggle="modal" data-target="#delete-product-modal-{{$product->id}}" class="text-danger"><i class="fas fa-trash"></i></a>

                        <div id="delete-product-modal-{{$product->id}}" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
                            <div role="document" class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 id="exampleModalLabel" class="modal-title">{{__('Product verwijderen')}}?</h5>
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            {{__('Weet u zeker dat u')}} <b>{{$product->title}}</b> {{__('wilt verwijderen')}}?
                                        </p>
                                        @if (!empty($product->image))

                                            <img src="/{{$product->image}}" alt="{{$product->title}}" class="img-fluid">
                                        @endif
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-secondary">{{__('Annuleren')}}</button>
                                        <a href="{{route('products.delete', $product->id)}}" class="btn btn-danger">{{__('Verwijderen')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
