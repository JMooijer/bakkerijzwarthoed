<table class="table vats-table">
    <thead>
        <tr>
            <th>{{__('Titel')}}</th>
            <th>{{__('Tarief')}}</th>
            <th>{{__('Default')}}</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($vats as $vat)
            
            <tr>                                
                <td>
                    <a href="{{route('vats.edit', $vat->id,)}}">{{$vat->title}}</a>
                </td>
                <td>{{$vat->rate}}%</td>
                <td>{{$vat->default ? 'Ja' : '-'}}</td>
                <td>
                    <a href="#" type="button" data-toggle="modal" data-target="#delete-vat-modal-{{$vat->id}}" class="text-danger"><i class="fas fa-trash"></i></a>

                    <div id="delete-vat-modal-{{$vat->id}}" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
                        <div role="document" class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 id="exampleModalLabel" class="modal-title">{{__('BTW tarief verwijderen')}}?</h5>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        {{__('Weet u zeker dat u')}} <b>{{$vat->title}}</b> {{__('wilt verwijderen')}}?
                                    </p>                                                             
                                </div>
                                <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-secondary">{{__('Annuleren')}}</button>
                                    <a href="{{route('vats.delete', $vat->id)}}" class="btn btn-danger">{{__('Verwijderen')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>