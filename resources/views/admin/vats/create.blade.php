@extends('layouts.dashboard')

@section('content')

    <div class="container mt-5 pb-3">
        <div class="row">
            <div class="col-12">
                <a href="{{route('vats.index')}}"><i class="fas fa-arrow-left"></i> {{__('Terug')}}</a>
                <div class="card mt-3">
                    <div class="card-body">
                        <div class="card-title">
                            <h2>{{__('BTW tarief toevoegen')}}</h2>
                        </div>
                        <hr/>
                        <form action="{{route('vats.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
        
                            <div class="form-group">
                                <label for="title">{{__('Titel')}}</label>
                                <input type="text" name="title" class="form-control @error('title') is-invalid @enderror"/>

                                @error('title')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="rate">{{__('Tarief')}}</label>
                                <input type="number" name="rate" min="0" step=".01" class="form-control @error('rate') is-invalid @enderror"/>

                                @error('rate')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="default" id="default">
                                <label class="form-check-label" for="default">{{__('Default')}}</label>

                                @error('default')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>
        
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">{{__('Opslaan')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection