@extends('layouts.dashboard')

@section('content')

    <div class="container mt-5 pb-3">
        <div class="row">
            <div class="col-12">
                <a href="{{route('users.index')}}"><i class="fas fa-arrow-left"></i> {{__('Terug')}}</a>
                <div class="card mt-3">
                    <div class="card-body">
                        <div class="card-title">
                            <h2>{{__('Gebruiker toevoegen')}}</h2>
                        </div>
                        <hr/>
                        <form action="{{route('users.store')}}" method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="name">{{__('Naam')}}</label>
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"/>

                                @error('name')
                                <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">{{__('E-mailadres')}}</label>
                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"/>

                                @error('email')
                                <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password">{{__('Wachtwoord')}}</label>
                                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror"/>

                                @error('password')
                                <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">{{__('Opslaan')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
