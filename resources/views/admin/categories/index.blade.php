@extends('layouts.dashboard')

@section('content')

<div class="container mt-5 pb-3">

    @if (session('message'))
        <div class="row">
            <div class="col-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('message')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-4">
            <a href="{{route('dashboard')}}"><i class="fas fa-arrow-left"></i> {{__('Terug')}}</a>
        </div>
        <div class="col-8 mb-3 text-right">
            <a href="{{route('categories.create')}}" class="btn btn-primary mb-3"><i class="fas fa-plus"></i> {{__('Categorie toevoegen')}}</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="category-search">{{__('Zoeken')}}</label>
                                <input type="text" name="category-search" class="form-control" id="category-search"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">{{__('Categorieën')}}</h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table categories-table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>{{__('Titel')}}</th>
                                    <th>{{__('Beschrijving')}}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)

                                    <tr>
                                        <td>
                                            @if (!empty($category->image))

                                                <img src="/{{ $category->image }}" alt="{{$category->title}}" class="img-fluid"/>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('categories.edit', $category->id,)}}">{{$category->title}}</a>
                                        </td>
                                        <td>{!!$category->description!!}</td>
                                        <td>
                                            <a href="#" type="button" data-toggle="modal" data-target="#delete-category-modal-{{$category->id}}" class="text-danger"><i class="fas fa-trash"></i></a>

                                            <div id="delete-category-modal-{{$category->id}}" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
                                                <div role="document" class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 id="exampleModalLabel" class="modal-title">{{__('Categorie verwijderen')}}?</h5>
                                                            <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>
                                                                {{__('Weet u zeker dat u')}} <b>{{$category->title}}</b> {{__('wilt verwijderen?')}}?
                                                            </p>
                                                            @if (!empty($category->image))

                                                                <img src="/{{$category->image}}" alt="{{$category->title}}" class="img-fluid">
                                                            @endif
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" data-dismiss="modal" class="btn btn-secondary">{{__('Annuleren')}}</button>
                                                            <a href="{{route('categories.delete', $category->id)}}" class="btn btn-danger">{{__('Verwijderen')}}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
