@extends('layouts.dashboard')

@section('content')

    <div class="container mt-5 pb-3">
        <div class="row">
            <div class="col-12">
                <a href="{{route('categories.index')}}"><i class="fas fa-arrow-left"></i> {{__('Terug')}}</a>
                <div class="card mt-3">
                    <div class="card-body">
                        <div class="card-title">
                            <h2>{{__('Categorie toevoegen')}}</h2>
                        </div>
                        <hr/>
                        <form action="{{route('categories.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
        
                            <div class="form-group">
                                <label for="title">{{__('Titel')}}</label>
                                <input type="text" name="title" class="form-control @error('title') is-invalid @enderror"/>

                                @error('title')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>
        
                            <div class="form-group">
                                <label for="description">{{__('Beschrijving')}}</label>
                                <textarea name="description" cols="30" rows="10" class="form-control ckeditor @error('description') is-invalid @enderror"></textarea>

                                @error('description')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="image">{{__('Afbeelding')}}</label>
                                <input type="file" name="image" class="form-control-file @error('description') is-invalid @enderror" id="image">

                                @error('image')
                                    <div class="alert alert-danger mt-3">{{ $message }}</div>
                                @enderror
                            </div>
        
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection