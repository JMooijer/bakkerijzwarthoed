<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProductsTableAndAlterVatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->decimal('price', 7, 2)->change();
        });

        Schema::table('vats', function (Blueprint $table) {
            $table->decimal('rate', 5, 2)->change();
        });
    }
}
