<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('payment_status');
            $table->string('mollie_id')->nullable();
            $table->string('paid_at')->nullable();
            $table->decimal('amount', 7, 2)->nullable();
            $table->string('customer_email');
            $table->string('customer_first_name');
            $table->string('customer_last_name');
            $table->string('invoice_address');
            $table->string('invoice_address_addition');
            $table->string('invoice_place');
            $table->string('invoice_zipcode');
            $table->string('shipping_address')->nullable();;
            $table->string('shipping_address_addition')->nullable();;
            $table->string('shipping_place')->nullable();;
            $table->string('shipping_zipcode')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
