<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            [
                'email'     => 'johnmooyer@hotmail.com',
                'name'      => 'Johnny Mooijer',
                'password'  => Hash::make('test1234'),
            ],
        ]);
    }
}
