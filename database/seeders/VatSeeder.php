<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class VatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vats')->insert([

            [
                'title'     => 'NL Laag',
                'rate'      => '9',
                'default'   => 1,
            ],
        ]);
    }
}
